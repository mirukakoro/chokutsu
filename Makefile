local: server
	heroku local

server: go.mod
	go build -o ./server ./cmd/server
