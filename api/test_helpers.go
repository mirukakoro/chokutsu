package api

import "testing"

const (
	defaultAgencyTag = "ttc"
	defaultRouteTag  = "7"
)

func HelpTestAPI_Agencies(a API, t *testing.T) {
	agencies, licensing, err := a.Agencies()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("licensing: %s", licensing)
	t.Log(agencies)
}

func HelpTestAPI_Routes(a API, t *testing.T) {
	routes, licensing, err := a.Routes(defaultAgencyTag)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("licensing: %s", licensing)
	t.Log(routes)
}
