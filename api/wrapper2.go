package api

import (
	"context"
	"fmt"
	"gitlab.com/mirukakoro/chokutsu/proto"
	"google.golang.org/grpc"
	"time"
)

type wrapper2 struct {
	client proto.DataClient
}

func Wrap2(client proto.DataClient) API {
	return &wrapper2{client: client}
}

var _ API = (*wrapper2)(nil)

func (w *wrapper2) String() string {
	prefix := fmt.Sprintf("External API (using gRPC %s)", grpc.Version)
	name, err := w.client.Name(context.Background(), &proto.Nothing{})
	if err != nil {
		return fmt.Sprintf("%s %s", prefix, err)
	}
	return fmt.Sprintf("%s %s %s", prefix, name.Name, name.Version)
}

func (w *wrapper2) Agencies() (agencies []*proto.Agency, licensing string, err error) {
	resp, err := w.client.Agencies(context.Background(), &proto.Nothing{})
	if err != nil {
		return
	}
	return resp.Agencies, resp.Licensing, nil
}

func (w *wrapper2) Routes(agencyTag string) (routes []*proto.Route, licensing string, err error) {
	resp, err := w.client.Routes(context.Background(), &proto.RoutesReq{AgencyTag: agencyTag})
	if err != nil {
		return
	}
	return resp.Routes, resp.Licensing, nil
}

func (w *wrapper2) Route(agencyTag, routeTag string) (route *proto.Route, licensing string, err error) {
	resp, err := w.client.Route(context.Background(), &proto.RouteReq{
		AgencyTag: agencyTag,
		RouteTag:  routeTag,
	})
	if err != nil {
		return
	}
	return resp.Route, resp.Licensing, nil
}

func (w *wrapper2) Locations(agencyTag, routeTag string, t time.Time) (locations []*proto.VehicleLocation, licensing string, err error) {
	resp, err := w.client.LocationsRoute(context.Background(), &proto.LocationsRouteReq{
		AgencyTag: agencyTag,
		RouteTag:  routeTag,
		T:         t.Format(time.RFC3339),
	})
	if err != nil {
		return
	}
	return resp.Locations, resp.Licensing, nil
}

func (w *wrapper2) Location(agencyTag, vehicleTag string) (location *proto.VehicleLocation, licensing string, err error) {
	resp, err := w.client.Locations(context.Background(), &proto.LocationsReq{
		AgencyTag:   agencyTag,
		VehicleTags: []string{vehicleTag},
	})
	if err != nil {
		return
	}
	return resp.Locations[0], resp.Licensing, nil
}

func (w *wrapper2) Predictions(agencyTag string, rsTags ...*proto.RouteStopTag) (predictions []*proto.Prediction, licensing string, err error) {
	resp, err := w.client.Predictions(context.Background(), &proto.PredictionsReq{
		AgencyTag: agencyTag,
		RsTags:    rsTags,
	})
	if err != nil {
		return
	}
	return resp.Predictions, resp.Licensing, nil
}

func (w *wrapper2) Messages(agencyTag string, routeTags ...string) (messages []*proto.Message, licensing string, err error) {
	resp, err := w.client.Messages(context.Background(), &proto.MessagesReq{
		AgencyTag: agencyTag,
		RouteTags: routeTags,
	})
	if err != nil {
		return
	}
	return resp.Messages, resp.Licensing, nil
}
