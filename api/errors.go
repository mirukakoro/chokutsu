package api

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	ErrNotFound       = errors.New("not found")
	ErrNotFoundAgency = fmt.Errorf("agency %w", ErrNotFound)
)

func StatusFromErr(err error) int {
	if err == nil {
		return http.StatusOK
	}
	if errors.Is(err, ErrNotFound) {
		return http.StatusNotFound
	}
	return http.StatusInternalServerError
}
