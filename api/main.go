package api

import (
	"errors"
	"fmt"
	"gitlab.com/mirukakoro/chokutsu/proto"
	"time"
)

// ErrNotImplemented (or an error that contains ErrNotImplemented i.e. errors.Is(err, ErrNotImplemented) is true)
// should be returned if a function is not implemented.
var ErrNotImplemented = errors.New("not implemented")

type API interface {
	fmt.Stringer

	// Agencies returns a list of agencies.
	// The RouteTag field of each proto.Agency must be non-zero.
	Agencies() (agencies []*proto.Agency, licensing string, err error)

	// Routes returns a list of routes an agency provides.
	Routes(agencyTag string) (routes []*proto.Route, licensing string, err error)

	// Route returns detailed info about a route.
	// All fields of the proto.Route must be non-zero.
	Route(agencyTag, routeTag string) (route *proto.Route, licensing string, err error)

	// Locations returns the locations of vehicles on route routeTag (of agency agencyTag).
	// t specifies the last time that was returned. (quoted from the Umo IQ documentation).
	Locations(agencyTag, routeTag string, t time.Time) (locations []*proto.VehicleLocation, licensing string, err error)

	// Location returns the location of vehicle vehicleTag.
	Location(agencyTag, vehicleTag string) (location *proto.VehicleLocation, licensing string, err error)

	// Predictions returns the predictions of vehicles.
	Predictions(agencyTag string, rsTags ...*proto.RouteStopTag) (predictions []*proto.Prediction, licensing string, err error)

	// Messages returns the messages active for routes.
	Messages(agencyTag string, routeTags ...string) (messages []*proto.Message, licensing string, err error)
}
