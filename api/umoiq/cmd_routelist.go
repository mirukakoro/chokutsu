package umoiq

import (
	"net/url"
)

// RouteListReq makes an url.Values for getting predictions using an agencyTag.
func (a *base) RouteListReq(agencyTag string) url.Values {
	q := a.Base.Query()
	q.Set("command", "routeList")
	q.Set("a", agencyTag)
	return q
}

// RouteList returns the list of routes an agency has.
func (a *base) RouteList(agencyTag string) (routeList *RouteListResp, err error) {
	routeList = &RouteListResp{}
	return routeList, a.get(a.RouteListReq(agencyTag), routeList)
}

type RouteListResp struct {
	BaseResp
	Routes []Route `xml:"route"`
}
