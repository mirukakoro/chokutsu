package umoiq

import (
	"net/url"
)

type RouteStopTag struct {
	RouteTag string
	StopTag  string
}

func (t RouteStopTag) forQuery() string {
	return t.RouteTag + "|" + t.StopTag
}

// PredictionsReq makes an url.Values for getting predictions using an agencyTag, routeTag, and stopTag.
// It uses the `predictions` command instead of the `predictionsForMultiStops` command
// if there is only one RouteStopTag.
func (a *base) PredictionsReq(agencyTag string, rsTags ...RouteStopTag) url.Values {
	q := a.Base.Query()
	if len(rsTags) == 0 {
		q.Set("command", "predictions")
		q.Set("a", agencyTag)
		q.Set("r", rsTags[0].RouteTag)
		q.Set("s", rsTags[0].StopTag)
	} else {
		q.Set("command", "predictionsForMultiStops")
		q.Set("a", agencyTag)
		for _, rsTag := range rsTags {
			q.Add("stops", rsTag.forQuery())
		}
	}
	return q
}

// Predictions returns the predictions for a stop.
func (a *base) Predictions(agencyTag string, rsTags ...RouteStopTag) (predictions *PredictionsResp, err error) {
	predictions = &PredictionsResp{}
	return predictions, a.get(a.PredictionsReq(agencyTag, rsTags...), predictions)
}

type PredictionsResp struct {
	BaseResp
	Predictions []PredictionsRespChild `xml:"predictions"`
}

type PredictionsRespChild struct {
	AgencyTitle string              `xml:"agencyTitle,attr"`
	RouteTitle  string              `xml:"routeTitle,attr"`
	RouteTag    string              `xml:"routeTag,attr"`
	StopTitle   string              `xml:"stopTitle,attr"`
	StopTag     string              `xml:"stopTag,attr"`
	Direction   PredictionDirection `xml:"direction"`
	Messages    []Message           `xml:"message"`
}
