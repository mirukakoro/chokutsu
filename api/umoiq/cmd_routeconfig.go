package umoiq

import (
	"net/url"
)

// RouteConfigReq makes an url.Values for getting predictions using an agencyTag, and routeTag.
func (a *base) RouteConfigReq(agencyTag, routeTag string) url.Values {
	q := a.Base.Query()
	q.Set("command", "routeConfig")
	q.Set("a", agencyTag)
	q.Set("r", routeTag)
	return q
}

// RouteConfig returns information about a route.
func (a *base) RouteConfig(agencyTag, routeTag string) (routeConfig *RouteConfigResp, err error) {
	routeConfig = &RouteConfigResp{}
	return routeConfig, a.get(a.RouteConfigReq(agencyTag, routeTag), routeConfig)
}

type RouteConfigResp struct {
	BaseResp
	Route Route `xml:"route"`
}
