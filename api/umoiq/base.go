package umoiq

import (
	"encoding/xml"
	"fmt"
)

type (
	Error struct {
		Text        string `xml:",chardata"`
		ShouldRetry bool   `xml:"shouldRetry,attr"`
	}
	BaseResp struct {
		XMLName   xml.Name `xml:"body"`
		Copyright string   `xml:"copyright,attr"`
		Error     Error    `xml:"Error"`
	}
)

type Agency struct {
	Tag    string  `json:"tag" xml:"tag,attr"`
	Title  string  `json:"title" xml:"title,attr"`
	Region string  `json:"region" xml:"regionTitle,attr"`
	Short  string  `json:"short" xml:"shortTitle,attr"`
	Routes []Route `json:"routes,omitempty"`
}

func (a Agency) String() string {
	return fmt.Sprintf("agency %s (%s)", a.Tag, a.ShortIf())
}

func (a Agency) ShortIf() string {
	if a.Short != "" {
		return a.Short
	}
	return a.Region + " " + a.Title
}

type Route struct {
	Copyright     string      `json:"copyright"`
	Tag           string      `json:"tag" xml:"tag,attr"`
	Title         string      `json:"title" xml:"title,attr"`
	Color         string      `json:"color" xml:"color,attr"`
	OppositeColor string      `json:"opposite_color" xml:"oppositeColor,attr"`
	LatMin        float32     `json:"lat_min,omitempty" xml:"latMin,attr"`
	LatMax        float32     `json:"lat_max,omitempty" xml:"latMax,attr"`
	LonMin        float32     `json:"lon_min,omitempty" xml:"lonMin,attr"`
	LonMax        float32     `json:"lon_max,omitempty" xml:"lonMax,attr"`
	Stops         []Stop      `json:"stops,omitempty" xml:"stop"`
	Directions    []Direction `json:"directions,omitempty" xml:"direction"`
	Paths         []Path      `json:"paths,omitempty" xml:"path"`
}

func (r Route) String() string {
	return fmt.Sprintf("route %s (%s)", r.Tag, r.Title)
}

func (r Route) LongString() string {
	stops := fmt.Sprintf("%d stop(s):\n", len(r.Stops))
	for _, stop := range r.Stops {
		stops += "\t" + stop.String() + "\n"
	}
	directions := fmt.Sprintf("%d direction(s):\n", len(r.Directions))
	for _, direction := range r.Directions {
		directions += "\t" + direction.String() + "\n"
	}
	paths := fmt.Sprintf("%d path(s):\n", len(r.Paths))
	for _, path := range r.Paths {
		paths += "\t" + path.String() + "\n"
	}
	return fmt.Sprintf("%s:\n%s\n%s\n%s", r.String(), stops, directions, paths)
}

type Stop struct {
	Tag    string  `xml:"tag,attr"`
	Title  string  `xml:"title,attr"`
	Lat    float32 `xml:"lat,attr"`
	Lon    float32 `xml:"lon,attr"`
	StopId string  `xml:"stopId,attr"`
}

func (s Stop) String() string {
	return fmt.Sprintf("stop %s (id %s, %s)", s.Tag, s.StopId, s.Title)
}

type Direction struct {
	Tag      string `xml:"tag,attr"`
	Title    string `xml:"title,attr"`
	Name     string `xml:"name,attr"`
	UseForUI bool `xml:"useForUI,attr"`
	Branch   string `xml:"branch,attr"`
	Stops    []Stop `xml:"stop"`
}

func (d Direction) String() string {
	return fmt.Sprintf("direction %s (%s)", d.Tag, d.Title)
}

type Path struct {
	Points []Point `xml:"point"`
}

func (p Path) String() string {
	return fmt.Sprintf("path with %d point(s)", len(p.Points))
}

type Point struct {
	Lat float32 `xml:"lat,attr"`
	Lon float32 `xml:"lon,attr"`
}

type PredictionDirection struct {
	// TODO: maybe embed Direction?
	Title      string       `xml:"title,attr"`
	Prediction []Prediction `xml:"prediction"`
}

type Prediction struct {
	EpochTime         uint64 `xml:"epochTime,attr"`
	Seconds           uint64 `xml:"seconds,attr"`
	Minutes           uint64 `xml:"minutes,attr"`
	IsDeparture       bool `xml:"isDeparture,attr"`
	AffectedByLayover bool `xml:"affectedByLayover,attr"`
	IsScheduleBased bool `xml:"isScheduleBased,attr"`
	Delayed bool `xml:"delayed,attr"`
	DirTag            string `xml:"dirTag,attr"`
	Vehicle           string `xml:"vehicle,attr"`
	VehiclesInConsist string `xml:"vehiclesInConsist,attr"`
	Block             string `xml:"block,attr"`
	TripTag           string `xml:"tripTag,attr"`
}
