package umoiq

import (
	"net/url"
	"strconv"
	"time"
)

// VehicleLocationsReq makes an url.Values for getting the locations of vehicles on a route.
func (a *base) VehicleLocationsReq(agencyTag, routeTag string, t time.Time) url.Values {
	q := a.Base.Query()
	q.Set("command", "vehicleLocations")
	q.Set("a", agencyTag)
	q.Set("r", routeTag)
	q.Set("t", strconv.FormatInt(int64(t.Nanosecond())*1e6, 10))
	return q
}

// VehicleLocationReq makes an url.Values for getting the location of a vehicle.
func (a *base) VehicleLocationReq(agencyTag, vehicleID string) url.Values {
	q := a.Base.Query()
	q.Set("command", "vehicleLocation")
	q.Set("a", agencyTag)
	q.Set("v", vehicleID)
	return q
}

func (a *base) VehicleLocations(agencyTag string, routeTag string, t time.Time) (vehicleLocations *VehicleLocationsResp, err error) {
	vehicleLocations = &VehicleLocationsResp{}
	return vehicleLocations, a.get(a.VehicleLocationsReq(agencyTag, routeTag, t), vehicleLocations)
}

func (a *base) VehicleLocation(agencyTag string, vehicleID string) (vehicleLocations *VehicleLocationsResp, err error) {
	vehicleLocations = &VehicleLocationsResp{}
	return vehicleLocations, a.get(a.VehicleLocationReq(agencyTag, vehicleID), vehicleLocations)
}

// VehicleLocationsResp is the response returned by the base for the vehicleLocations and vehicleLocation commands.
type VehicleLocationsResp struct {
	Vehicles  []Vehicle `xml:"vehicle"`
	LastTime struct {
		Time string `xml:"time,attr"`
	} `xml:"lastTime"`
}

type Vehicle struct {
	ID               string `xml:"id,attr"`
	RouteTag         string `xml:"routeTag,attr"`
	DirTag           string `xml:"dirTag,attr"`
	Lat              float32 `xml:"lat,attr"`
	Lon              float32 `xml:"lon,attr"`
	SecsSinceReport  int32  `xml:"secsSinceReport,attr"`
	Predictable      bool   `xml:"predictable,attr"`
	Heading          int32 `xml:"heading,attr"`
	SpeedKmHr        int32 `xml:"speedKmHr,attr"`
	LeadingVehicleId string `xml:"leadingVehicleId,attr"`
}
