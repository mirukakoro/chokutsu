package umoiq

import "gitlab.com/mirukakoro/chokutsu/proto"

func convertToAgencies(resp *AgencyListResp) (agencies []*proto.Agency, licensing string, err error) {
	licensing = resp.Copyright
	agencies = make([]*proto.Agency, len(resp.Agencies))
	for i, agency := range resp.Agencies {
		agencies[i], err = convertToAgency(&agency)
		if err != nil {
			return nil, "", err
		}
	}
	return
}

func convertToAgency(agency *Agency) (*proto.Agency, error) {
	routes, err := convertToRoutes(agency.Routes)
	if err != nil {
		return nil, err
	}
	return &proto.Agency{
		Tag:    agency.Tag,
		Title:  agency.Title,
		Region: agency.Region,
		Short:  agency.Short,
		Routes: routes,
	}, nil
}

func convertToRoutes(routes []Route) ([]*proto.Route, error) {
	routes2 := make([]*proto.Route, len(routes))
	var err error
	for i, route := range routes {
		routes2[i], err = convertToRoute(&route)
		if err != nil {
			return nil, err
		}
	}
	return routes2, nil
}

func convertToRoute(route *Route) (*proto.Route, error) {
	stops, err := convertToStops(route.Stops)
	if err != nil {
		return nil, err
	}
	directions, err := convertToDirections(route.Directions)
	if err != nil {
		return nil, err
	}
	paths, err := convertToPaths(route.Paths)
	if err != nil {
		return nil, err
	}
	return &proto.Route{
		Tag:           route.Tag,
		Title:         route.Title,
		Color:         route.Color,
		ColorOpposite: route.OppositeColor,
		Min: &proto.Point{
			Lat: route.LatMin,
			Lon: route.LonMin,
		},
		Max: &proto.Point{
			Lat: route.LatMax,
			Lon: route.LonMax,
		},
		Stops:      stops,
		Directions: directions,
		Paths:      paths,
	}, nil
}

func convertToStops(stops []Stop) ([]*proto.Stop, error) {
	stops2 := make([]*proto.Stop, len(stops))
	var err error
	for i, stop := range stops {
		stops2[i], err = convertToStop(&stop)
		if err != nil {
			return nil, err
		}
	}
	return stops2, nil
}

func convertToStop(stop *Stop) (*proto.Stop, error) {
	return &proto.Stop{
		Tag:   stop.Tag,
		Title: stop.Title,
		Id:    stop.StopId,
		Pos: &proto.Point{
			Lat: stop.Lat,
			Lon: stop.Lon,
		},
	}, nil
}

func convertToDirections(directions []Direction) ([]*proto.Direction, error) {
	directions2 := make([]*proto.Direction, len(directions))
	var err error
	for i, direction := range directions {
		directions2[i], err = convertToDirection(direction)
		if err != nil {
			return nil, err
		}
	}
	return directions2, nil
}

func convertToDirection(direction Direction) (*proto.Direction, error) {
	stops, err := convertToStops(direction.Stops)
	if err != nil {
		return nil, err
	}
	return &proto.Direction{
		Tag:      direction.Tag,
		Title:    direction.Title,
		Name:     direction.Name,
		Branch:   direction.Branch,
		UseForUI: direction.UseForUI,
		Stops:    stops,
	}, nil
}

func convertToPaths(paths []Path) ([]*proto.Path, error) {
	paths2 := make([]*proto.Path, len(paths))
	var err error
	for i, path := range paths {
		paths2[i], err = convertToPath(&path)
		if err != nil {
			return nil, err
		}
	}
	return paths2, nil
}

func convertToPath(path *Path) (*proto.Path, error) {
	points := make([]*proto.Point, len(path.Points))
	for i, point := range path.Points {
		points[i] = &proto.Point{
			Lat: point.Lat,
			Lon: point.Lon,
		}
	}
	return &proto.Path{Points: points}, nil
}

func convertToVehicleLocations(vehicles []Vehicle) ([]*proto.VehicleLocation, error) {
	vls := make([]*proto.VehicleLocation, len(vehicles))
	var err error
	for i, vehicle := range vehicles {
		vls[i], err = convertToVehicleLocation(&vehicle)
		if err != nil {
			return nil, err
		}
	}
	return vls, nil
}

func convertToVehicleLocation(vehicle *Vehicle) (*proto.VehicleLocation, error) {
	return &proto.VehicleLocation{
		Target: &proto.Vehicle{
			Tag:      vehicle.ID,
			RouteTag: vehicle.RouteTag,
			DirTag:   vehicle.DirTag,
		},
		Location: &proto.Point{
			Lat: vehicle.Lat,
			Lon: vehicle.Lon,
		},
		SecsSinceReport: vehicle.SecsSinceReport,
		Predictable:     vehicle.Predictable,
		Heading:         vehicle.Heading,
		Speed:           vehicle.SpeedKmHr,
		LeadingTag:      vehicle.LeadingVehicleId,
	}, nil
}

func convertToMessages(messages []Message) ([]*proto.Message, error) {
	messages2 := make([]*proto.Message, len(messages))
	var err error
	for i, message := range messages {
		messages2[i], err = convertToMessage(&message)
		if err != nil {
			return nil, err
		}
	}
	return messages2, nil
}

var priorityMap = map[string]int32{
	"Low":    0,
	"Normal": 1,
	"High":   2,
}

func convertToMessage(message *Message) (*proto.Message, error) {
	var priority int32
	if message.Priority2 != nil {
		priority = *message.Priority2
	} else {
		var ok bool
		priority, ok = priorityMap[message.Priority]
		if !ok {
			priority = -1
		}
	}
	forRoutes, err := convertToMessageRoutes(message.ForRoutes)
	if err != nil {
		return nil, err
	}
	return &proto.Message{
		Tag:         message.ID,
		Text:        message.Text,
		TextTTS:     message.TextTTS,
		TextSMS:     message.TextSMS,
		TextAlt:     message.TextAlt,
		Creator:     message.Creator,
		Priority:    priority,
		SendToBuses: message.SendToBuses,
		ForRoutes:   forRoutes,
	}, nil
}

func convertToMessageRoutes(routes []ForRoute) ([]*proto.MessageRoute, error) {
	mrs := make([]*proto.MessageRoute, len(routes))
	var err error
	for i, route := range routes {
		mrs[i], err = convertToMessageRoute(&route)
		if err != nil {
			return nil, err
		}
	}
	return mrs, nil
}

func convertToMessageRoute(f *ForRoute) (*proto.MessageRoute, error) {
	stops, err := convertToStops(f.Stops)
	if err != nil {
		return nil, err
	}
	return &proto.MessageRoute{
		TargetRouteTag: f.RouteTag,
		Stops:          stops,
	}, nil
}

func convertToPredictions(predictions []PredictionsRespChild) ([]*proto.Prediction, error) {
	preds := make([]*proto.Prediction, len(predictions))
	var preds2 []*proto.Prediction
	var err error
	for _, prediction := range predictions {
		preds2, err = convertToPrediction(&prediction)
		if err != nil {
			return nil, err
		}
		preds = append(preds, preds2...)
	}
	return preds, nil
}

func convertToPrediction(respChild *PredictionsRespChild) ([]*proto.Prediction, error) {
	preds := make([]*proto.Prediction, len(respChild.Direction.Prediction))
	messages, err := convertToMessages(respChild.Messages)
	if err != nil {
		return nil, err
	}
	for i, pred := range respChild.Direction.Prediction {
		preds[i] = &proto.Prediction{
			Epoch:             pred.EpochTime,
			IsDeparture:       pred.IsDeparture,
			AffectedByLayover: pred.AffectedByLayover,
			TripTag:           pred.TripTag,
			Messages:          messages,
			Dir: &proto.Direction{
				Tag: pred.DirTag,
			},
			V: &proto.Vehicle{
				Tag:      pred.Vehicle,
				RouteTag: respChild.RouteTag,
				DirTag:   pred.DirTag,
			},
			Vic:   pred.VehiclesInConsist,
			Block: pred.Block,
		}
	}
	return preds, nil
}
