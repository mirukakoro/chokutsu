package umoiq

import (
	"gitlab.com/mirukakoro/chokutsu/api"
	"gitlab.com/mirukakoro/chokutsu/proto"
	"time"
)

type API struct {
	*base
}

var _ api.API = (*API)(nil)

func Default() *API {
	return &API{base: baseDefault()}
}

func (a *API) String() string {
	return "Chokutsu Umo IQ API wrapper"
}

func (a *API) Agencies() (agencies []*proto.Agency, licensing string, err error) {
	resp, err := a.AgencyList()
	if err != nil {
		return
	}
	return convertToAgencies(resp)
}

func (a *API) Routes(agencyTag string) (routes []*proto.Route, licensing string, err error) {
	resp, err := a.RouteList(agencyTag)
	if err != nil {
		return
	}
	licensing = resp.Copyright
	routes, err = convertToRoutes(resp.Routes)
	return
}

func (a *API) Route(agencyTag, routeTag string) (route *proto.Route, licensing string, err error) {
	resp, err := a.RouteConfig(agencyTag, routeTag)
	if err != nil {
		return
	}
	licensing = resp.Copyright
	route, err = convertToRoute(&resp.Route)
	return
}

func (a *API) Locations(agencyTag, routeTag string, t time.Time) (locations []*proto.VehicleLocation, licensing string, err error) {
	resp, err := a.base.VehicleLocations(agencyTag, routeTag, t)
	if err != nil {
		return
	}
	locations, err = convertToVehicleLocations(resp.Vehicles)
	return
}

func (a *API) Location(agencyTag, vehicleTag string) (location *proto.VehicleLocation, licensing string, err error) {
	resp, err := a.base.VehicleLocation(agencyTag, vehicleTag)
	if err != nil {
		return
	}
	var locations []*proto.VehicleLocation
	locations, err = convertToVehicleLocations(resp.Vehicles)
	if err != nil {
		return
	}
	if len(locations) == 0 {
        return
    }
	location = locations[0]
	return
}

func (a *API) Predictions(agencyTag string, rsTags ...*proto.RouteStopTag) (predictions []*proto.Prediction, licensing string, err error) {
	rsTags2 := make([]RouteStopTag,len(rsTags))
	for i,rsTag:=range rsTags{
		rsTags2[i] = RouteStopTag{
			RouteTag: rsTag.RouteTag,
			StopTag:  rsTag.StopTag,
		}
	}
	resp, err := a.base.Predictions(agencyTag, rsTags2...)
	if err != nil {
		return
	}
	licensing=resp.Copyright
	predictions,err=convertToPredictions(resp.Predictions)
	if err != nil {
		return
	}
	return
}

func (a *API) Messages(agencyTag string, routeTags ...string) (messages []*proto.Message, licensing string, err error) {
	resp, err := a.base.Messages(agencyTag, routeTags...)
	if err != nil {
		return
	}
	licensing = resp.Copyright
	messages = make([]*proto.Message, 0)
	var messages2 []*proto.Message
	for _, route := range resp.Route {
		messages2, err = convertToMessages(route.Messages)
		if err != nil {
			return nil, "", err
		}
		messages = append(messages, messages2...)
	}
	return
}
