package umoiq

import "net/url"

// ScheduleReq makes an url.Values for getting the schedule of a route.
func (a *base) ScheduleReq(agencyTag, routeTag string) url.Values {
	q := a.Base.Query()
	q.Set("command", "schedule")
	q.Set("a", agencyTag)
	q.Set("r", routeTag)
	return q
}

func (a *base) Schedule(agencyTag, routeTag string) (schedule *ScheduleResp, err error) {
	schedule = &ScheduleResp{}
	return schedule, a.get(a.ScheduleReq(agencyTag, routeTag), schedule)
}

type ScheduleResp struct {
	BaseResp
	Route []struct {
		Tag           string `xml:"tag,attr"`
		Title         string `xml:"title,attr"`
		ScheduleClass string `xml:"scheduleClass,attr"`
		ServiceClass  string `xml:"serviceClass,attr"`
		Direction     string `xml:"direction,attr"`
		Header        struct {
			Stop []struct {
				Text string `xml:",chardata"`
				Tag  string `xml:"tag,attr"`
			} `xml:"stop"`
		} `xml:"header"`
		Tr []struct {
			BlockID string `xml:"blockID,attr"`
			Stop    []struct {
				Text      string `xml:",chardata"`
				Tag       string `xml:"tag,attr"`
				EpochTime string `xml:"epochTime,attr"`
			} `xml:"stop"`
		} `xml:"tr"`
	} `xml:"route"`
}
