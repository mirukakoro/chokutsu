package umoiq

import (
	"net/url"
)

// AgencyListReq makes an url.Values for getting predictions.
func (a *base) AgencyListReq() url.Values {
	q := a.Base.Query()
	q.Set("command", "agencyList")
	return q
}

// AgencyList returns the list of agencies available.
func (a *base) AgencyList() (agencyList *AgencyListResp, err error) {
	agencyList = &AgencyListResp{}
	return agencyList, a.get(a.AgencyListReq(), agencyList)
}

type AgencyListResp struct {
	BaseResp
	Agencies []Agency `xml:"agency"`
}
