package umoiq

import (
	"encoding/xml"
	"errors"
	"io"
	"net/http"
	"net/url"
	"time"
)

var DefaultBase, _ = url.Parse("https://retro.umoiq.com/service/publicXMLFeed")

type HTTPClient interface {
	Get(url string) (*http.Response, error)
}

type base struct {
	Base   *url.URL
	Client HTTPClient
}

func baseDefault() *base {
	return &base{
		Base:   DefaultBase,
		Client: &http.Client{Timeout: 1 * time.Second},
	}
}

func (a *base) get(query url.Values, v interface{}) (err error) {
	if v == nil {
		return errors.New("v is nil")
	}

	base := *a.Base
	base.RawQuery = query.Encode()
	resp, err := a.Client.Get(base.String())
	if err != nil {
		return
	}
	defer func(Body io.ReadCloser) {
		err2 := Body.Close()
		if err2 != nil {
			err = err2
		}
	}(resp.Body)

	err = xml.NewDecoder(resp.Body).Decode(v)
	if err != nil {
		return
	}
	return
}
