package umoiq

import "net/url"

func (a *base) MessagesReq(agencyTag string, routeTags ...string) url.Values {
	q := a.Base.Query()
	q.Set("command", "messages")
	q.Set("a", agencyTag)
	for _, routeTag := range routeTags {
		q.Set("r", routeTag)
	}
	return q
}

func (a *base) Messages(agencyTag string, routeTag ...string) (messages *MessagesResp, err error) {
	messages = &MessagesResp{}
	return messages, a.get(a.MessagesReq(agencyTag, routeTag...), messages)
}

type MessagesResp struct {
	BaseResp
	Route []struct {
		Tag      string    `xml:"tag,attr"`
		Messages []Message `xml:"message"`
	} `xml:"route"`
}

type Message struct {
	ID          string     `xml:"id,attr"`
	SendToBuses bool       `xml:"sendToBuses,attr"` // Show message on buses?
	Creator     string     `xml:"creator,attr"`
	Priority    string     `xml:"priority,attr"`         // Priority in text.
	Priority2   *int32       `xml:"priority"`              // Priority in numbers. Might not be filled.
	Text        string     `xml:"text"`                  // Message content.
	TextTTS     string     `xml:"phonemeText"`           // Text for TTS systems.
	TextSMS     string     `xml:"smsText"`               // Text for SMS only.
	TextAlt     string     `xml:"textSecondaryLanguage"` // Text in alternate language (e.g. French for Text, English for TextAlt in Quebec)
	ForRoutes   []ForRoute `xml:"routeConfiguredForMessage"`
	Intervals   []Interval `xml:"interval"`
}

type ForRoute struct {
	RouteTag string `xml:"tag,attr"`
	Stops    []Stop `xml:"stop"`
}

type Interval struct {
	StartDay  string `xml:"startDay,attr"`
	StartTime string `xml:"startTime,attr"`
	EndDay    string `xml:"endDay,attr"`
	EndTime   string `xml:"endTime,attr"`
}
