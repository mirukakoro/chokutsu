package umoiq

import (
	"gitlab.com/mirukakoro/chokutsu/api"
	"testing"
)

var a = Default()

func TestAPI_Agencies(t *testing.T) {
	api.HelpTestAPI_Agencies(a, t)
}

func TestAPI_Routes(t *testing.T) {
	api.HelpTestAPI_Routes(a, t)
}
