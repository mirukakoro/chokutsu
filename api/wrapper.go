package api

import (
	"context"
	"gitlab.com/mirukakoro/chokutsu/proto"
	"time"
)

type wrapper struct {
	proto.UnimplementedDataServer
	a API
}

func Wrap(api API) proto.DataServer {
	return &wrapper{
		a: api,
	}
}

func (a *wrapper) Name(_ context.Context, _ *proto.Nothing) (*proto.NameResp, error) {
	return &proto.NameResp{
		Name: a.a.String(),
	}, nil
}

var _ proto.DataServer = (*wrapper)(nil)

func (a *wrapper) Agencies(_ context.Context, _ *proto.Nothing) (resp *proto.AgenciesResp, err error) {
	resp = &proto.AgenciesResp{}
	resp.Agencies, resp.Licensing, err = a.a.Agencies()
	return
}

func (a *wrapper) Routes(_ context.Context, req *proto.RoutesReq) (resp *proto.RoutesResp, err error) {
	resp = &proto.RoutesResp{}
	resp.Routes, resp.Licensing, err = a.a.Routes(req.AgencyTag)
	return
}

func (a *wrapper) Route(_ context.Context, req *proto.RouteReq) (resp *proto.RouteResp, err error) {
	resp = &proto.RouteResp{}
	resp.Route, resp.Licensing, err = a.a.Route(req.AgencyTag, req.RouteTag)
	return
}

func (a *wrapper) LocationsRoute(_ context.Context, req *proto.LocationsRouteReq) (resp *proto.LocationsResp, err error) {
	resp = &proto.LocationsResp{}
	t, err := time.Parse(time.RFC3339,req.T)
	if err != nil {
		return nil, err
	}
	resp.Locations, resp.Licensing, err = a.a.Locations(req.AgencyTag, req.RouteTag, t)
	return
}

func (a *wrapper) Locations(_ context.Context, req *proto.LocationsReq) (resp *proto.LocationsResp, err error) {
	resp = &proto.LocationsResp{}
	var licensing string
	var location *proto.VehicleLocation
	for i, vehicleTag := range req.VehicleTags {
		location, licensing, err = a.a.Location(req.AgencyTag, vehicleTag)
		if err != nil {
			return nil, err
		}
		if location != nil {
            resp.Locations = append(resp.Locations, location)
            resp.Licensing += licensing
            if i != len(req.VehicleTags)-1 {
                resp.Licensing += "\n"
            }
        }
	}
	return
}

func (a *wrapper) Predictions(_ context.Context, req *proto.PredictionsReq) (resp *proto.PredictionsResp, err error) {
	resp = &proto.PredictionsResp{}
	resp.Predictions, resp.Licensing, err = a.a.Predictions(req.AgencyTag, req.RsTags...)
	return
}

func (a *wrapper) Messages(_ context.Context, req *proto.MessagesReq) (resp *proto.MessagesResp, err error) {
	resp = &proto.MessagesResp{}
	resp.Messages, resp.Licensing, err = a.a.Messages(req.AgencyTag, req.RouteTags...)
	return
}
