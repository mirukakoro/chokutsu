package server

import (
	"fmt"
	"gitlab.com/mirukakoro/chokutsu/api"
	"gitlab.com/mirukakoro/chokutsu/api/umoiq"
	"gitlab.com/mirukakoro/chokutsu/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

func Main(port uint) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return fmt.Errorf("listen: %w", err)
	}
	s := grpc.NewServer()
	a := api.Wrap(umoiq.Default())
	proto.RegisterDataServer(s, a)
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		return fmt.Errorf("serve: %w", err)
	}
	return nil
}
