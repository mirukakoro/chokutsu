module gitlab.com/mirukakoro/chokutsu

// +heroku goVersion go1.16
// ^ heroku please fix this…
go 1.16

require (
	github.com/golang/protobuf v1.5.0
	golang.org/x/sys v0.0.0-20200720211630-cb9d2d5c5666 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
