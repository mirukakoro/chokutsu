package main

import (
	"flag"
	"fmt"
	"gitlab.com/mirukakoro/chokutsu/proto/server"
	"os"
)

func main() {
	var port uint
	flag.UintVar(&port,"port",8080,"port to listen on")
	flag.Parse()
	err := server.Main(port)
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err)
		os.Exit(0)
	}
}
